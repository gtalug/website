var exampleLoc = ol.proj.transform(
    [-79.3773, 43.6577], 'EPSG:4326', 'EPSG:3857');

var map = new ol.Map({
    target: 'map',
    layers: [
      new ol.layer.Tile({
        source: new ol.source.OSM()
      })
    ],
    view: new ol.View({
      center: ol.proj.fromLonLat([-79.3773, 43.6577]),
      zoom: 16
    }),
});

var marker = new ol.Overlay({
    element: document.getElementById('location'),
    positioning: 'bottom-left',
    stopEvent: false
  });

function add_map_point(lat, lng) {
  var vectorLayer = new ol.layer.Vector({
    source:new ol.source.Vector({
      features: [
        new ol.Feature({
            geometry: new ol.geom.Point(
                ol.proj.transform([parseFloat(lng), parseFloat(lat)],
                    'EPSG:4326', 'EPSG:3857')),
        })]
    }),
    style: new ol.style.Style({
      image: new ol.style.Icon({
        anchor: [0.5, 0.5],
        anchorXUnits: "fraction",
        anchorYUnits: "fraction",
        src: "https://upload.wikimedia.org/wikipedia/commons/e/ec/RedDot.svg"
        }),

    text: new ol.style.Text({
        text: '245 Church Street,\nRoom 203 (second floor)',
        font: 'bold 14px FontAwesome',
        textAlign: 'center',
        textBaseline: 'bottom',
        offsetY: -12,
        padding: [5, 2, 2, 5],
        backgroundFill: new ol.style.Fill({
            color: 'white',
        }),
        fill: new ol.style.Fill({
            color: 'black',
        })
    }),

    })
  });
  map.addLayer(vectorLayer);
}

add_map_point(43.6577, -79.3773)