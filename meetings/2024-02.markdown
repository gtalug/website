meeting_title: "The Future of Linux User Groups and Open Source Advocacy"
meeting_datetime: 2024-02-09T11:00:00
meeting_location: "Big Blue Button - https://blue.lpi.org/b/eva-zjc-gjy-kgl"
meeting_speaker: " Stefano Maffulli, Executive Directorof the Open Source Initiative"
meeting_schedule_template: _schedules/virtual.html

Join GTALUG and our guest this month, Stefano Maffulli, to discuss the current state of Linux User Groups and Open Source advocacy, and how our community can be most effective in the coming years.

Stafano is Executive Director of the Open Source Initiative (OSI, opensource.org), maintainers of the Open Source Definition, and involved in ongoing advocacy and community development worldwide.

PLEASE NOTE THE MEETING DATE/TIME. To address timezone and availability issues, Stefano's live presentation will take place Friday February 9 at 11:00am Toronto time (16:00 UTC). 

A recording of the presentation as well as followup Q&A will be held during the usual GTALUG meeting time Tuesday February 13 at 19:30 Toronto time (Feb 14 at 00:30 UTC).

We're going to use Big BLue Button for this meeting:  https://blue.lpi.org/b/eva-zjc-gjy-kgl

**Time:**  February 09th, 2024 11:00 AM Eastern Time

**Join us on Big Blue Button:**  https://blue.lpi.org/b/eva-zjc-gjy-kgl.