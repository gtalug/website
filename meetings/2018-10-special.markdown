meeting_title: "Seatbelts and Airbags for Bash"
meeting_datetime: 2018-10-17T18:45:00
meeting_speaker: "Michael Potter"
meeting_location: "Hacklab.TO. 1266 Queen Street WestSuite #6 "


Hacklab.to & GTALUG present: Seatbelts and Airbags for Bash w/ Michael Potter

Please register for the event, registration is free -- [Eventbright page](https://www.eventbrite.ca/e/seatbelts-and-airbags-for-bash-tickets-51177497272)

# Abstract:
This presentation will focus on the underutilized features of bash that are critical to building production quality scripts. Demos will show you how to turn on options and error trapping that expose the hidden time bombs in the code.


Turning these features on should be considered a requirement, much like turning on “strict” and “warnings” is considered a requirement for Perl programs.

Also, as much of the bash syntax has become outdated, the presentation will explain which syntax is best to use and which syntax to avoid.

Knowledge of any of the common UNIX scripting languages will be sufficient to understand the presentation and script authors at all proficiency levels should find value in attending. Although the presentation does not address differences between Korn shell and bash, much of what will be discussed also applies to Korn shell.

# Bio:
Michael Potter is a long time UNIX user and C developer and more recently a consultant to insurance companies. He is a proponent of strongly typed languages which inspired him to try to "fix" bash by using its underutilized features to make it more reliable.


Please arrive early to be seated, presentation will start at 7pm sharp.
