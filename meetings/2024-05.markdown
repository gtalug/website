meeting_title: "Mailman 3, is it worth it? The trials and tribulations of upgrading, & do we really need mailing lists anymore? Discuss!"
meeting_datetime: 2024-05-14T19:30:00
meeting_location: "Big Blue Button - https://blue.lpi.org/b/eva-zjc-gjy-kgl"
meeting_speaker: "Ron Barnes"
meeting_schedule_template: _schedules/virtual.html



We're going to use Big BLue Button for this meeting:  https://blue.lpi.org/b/eva-zjc-gjy-kgl

**Time:**  May 14th, 2024 19:30 AM Eastern Time

**Join us on Big Blue Button:**  https://blue.lpi.org/b/eva-zjc-gjy-kgl.