meeting_title: "Windows subsystem for linux and Lightning talks"
meeting_datetime: 2021-04-13T19:30:00
meeting_location: "Zoom meeting"
meeting_schedule_template: _schedules/virtual.html


###  I've ditched my Linux desktops for Windows .... sorta. With Evan Leibovitch

After one too many times fighting with PulseAudio and video drivers and having printers and scanners that will NEVER work with Linux, I gave up my dual boot laptop and desktop and have loaded Win 10 on both.

Turns out, things run faster too, even open source apps like Handbrake which take advantage of the GPU to do its video decoding while Linux can't do that.

But the story is not done. I installed Windows Subsystem for Linux and now have a fully running Kubuntu as a VM when I want it. And there are some interesting tools such as Windows Terminal in which you can have a Linux terminal shell next to a tab with Windows PowerShell.


### Lightning talks

This is GTALUG's version of an un-conference, a loosely structured short talks emphasizing the informal exchange of information and ideas between participants, rather than following a conventionally structured GTALUG meetings.

If you already have a topic in mind please send an email to  [speakers@gtalug.org](mailto:speakers@gtalug.org) to be added to the list of scheduled talks.


We're going to use Zoom for this meeting:


**Time:** April 13, 2021 07:30 PM Easter Time

**Join Zoom Meeting:** [https://us02web.zoom.us/j/84001023881?pwd=Tk9DNnB0OXZibTJUT2hUR2taeGY2dz09](https://us02web.zoom.us/j/84001023881?pwd=Tk9DNnB0OXZibTJUT2hUR2taeGY2dz09)

**Meeting ID:** 840 0102 3881

**Passcode:** 688539

**Find your local number:** [https://us02web.zoom.us/u/kdNFVrX48s](https://us02web.zoom.us/u/kdNFVrX48s)