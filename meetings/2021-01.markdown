meeting_title: " Using OpenEmbedded Linux to automate and control the wood-fired heating of his home"
meeting_datetime: 2021-01-12T19:30:00
meeting_speaker: "Trevor Woerner"
meeting_location: "Zoom meeting"
meeting_schedule_template: _schedules/virtual.html


Trevor Woerner will be presenting his recent work using OpenEmbedded
Linux to automate and control the wood-fired heating of his home.

Bio:

Trevor Woerner is a senior software developer with deep knowledge of
deploying embedded Linux systems. He lives on a farm outside Toronto.
His technical blog is at: https://twoerner.blogspot.com/


We will be meeting on Zoom:

* [Join Zoom Meeting](https://us02web.zoom.us/j/88449213217?pwd=MjlMT2VHRFFqbE9RMVB3SDVlSk5yUT09)
* Meeting ID: 884 4921 3217
* Passcode: 658008
* [Find your local dial-in number](https://us02web.zoom.us/u/kvVXMQGoD)
