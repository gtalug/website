meeting_title: "Lightning Talks"
meeting_datetime: 2020-01-14T19:30:00
meeting_location: "George Vari Engineering and Computing Centre, Room 203"
meeting_location_template: _locations/r-gvecc-203.html
meeting_schedule_template: _schedules/2015-10.html
meeting_youtube_playlist: PLUgE6dqIXiEukZ9-hlcBfMniy_avsB6bQ

_Lighting Talks_ is GTALUG's version of an un-conference, a loosely structured short talks emphasizing the informal exchange of information and ideas between participants, rather than following a conventionally structured GTALUG meetings.

If you already have a topic in mind please send an email <a href="mailto:speakers@gtalug.org">speakers@gtalug.org</a> to be added to the list of scheduled talks.

If you would like to give a talk during the meeting, all you have to do is write your name on a post-it and stick it on the whiteboard in the front of the room. When your talk gets selected, you will have five to present your idea, then five minutes of audience questions.


### Five Minute Talks

* What I've learned about Ultra HD with D. Hugh Redelmeier
* Chris' intro to Kubernetes
* ZEO Bedside 2020 with Seneca Cunningham
* LUG community organizing and meetup.com with Alex Volkov