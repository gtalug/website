meeting_title: "Update Strategies for Embedded Devices"
meeting_datetime: 2022-03-08T19:30:00
meeting_location: "Virtual - Big Blue Button"
meeting_speaker: "Trevor Woerner"
meeting_schedule_template: _schedules/virtual.html

For various reasons, the strategies for performing a software update on an embedded device have diverged from what is commonly done on the desktop. This talk will discuss why, introduce the most common embedded strategies currently in use, and how to employ them.

We're going to use Big Blue Button for this meeting:

**Time:**  March 8, 2022 07:30 PM Eastern Time

**Join Big Blue Button:** https://blue.lpi.org/b/eva-zjc-gjy-kgl
